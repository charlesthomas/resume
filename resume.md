# Charles Thomas
[ch@rlesthom.as](mailto:ch@rlesthom.as)
###B.S. Computer Network and System Administration
**Michigan Technological University - Graduated 2008**

## Personal Projects
**Web Apps**

* [Wifi Display Maker](http://wifidisplaymaker.com); makes a simple printout for sharing your home wifi credentials
* [bufferti.me](http://www.bufferti.me); a better way to make Buffer schedules, using the [Buffer App API](http://bufferapp.com)
* [rou.st](http://www.rou.st); an urban exploration tool

**Python Libraries**

* [coinshot](https://pypi.python.org/pypi/coinshot); a library for the [pushover.net](http://pushover.net) API
* [moth](https://pypi.python.org/pypi/moth); an email-only authentication scheme
* [proauth2](https://pypi.python.org/pypi/proauth2); an OAuth2 provider

## Work Experience
### Senior Software Engineer, QA - Shopwiki.com - Aug 2012 to Present
**Responsibilities**

* QA team lead
	* Assign cases & projects to QA Team members
	* Train new QA team members
* Write Python tests for new UI features on [shopwiki.com](http://shopwiki.com), [compare.com](http://compare.com), and [rebatecove.com](http://rebatecove.com)
* Maintain existing UI test suite for same
* Work closely with development team to resolve bugs

**Accomplishments**

* Built system for collecting performance data using open source tools
* Reduced false-negative tests in UI testing suite by over 75%
* Reduced testing time per release from multiple days to a few hours
* Completed QA testing cycle for over 60 major and minor UI releases
* Built the entire Selenium test framework for [compare.com](http://compare.com)

### Application Operations Engineer - CheetahMail - Aug 2010 to Aug 2012
**Responsibilities**

* Develop custom internal tools for increasing team efficiency
* Troubleshoot / investigate & report application defects
* Monitor system performance; investigate abnormalities
* Create / update / maintain internal support documentation
* Train / assist team members
* Assist in major software releases

**Accomplishments**

* Created 4 Perl tools to increase team efficiency
	* Saved approximately 3 hours per ticket with one tool
	* Enabled pulling of previously unobtainable data with another
* Won 2011 Q1 Experian CheetahMail Pinnacle Award for individual performance
* Trained entire remote-office support team during their on-boarding
* Created or updated approximately 35 individual internal support wikis
	* Top 20 historical contributor

### Senior Support Analyst - TimeLink International - Jun 2008 to Aug 2010
**Responsibilities**

* Test and deploy software upgrades
	* Create additional database scripts for custom functions
* Migrate customer databases for local testing
* Work with development team to identify bugs
* Test and repair custom Linux embedded devices
* Train new support team members on all items listed above
	* Create training documentation
* Act as escalation point for less experienced members of the support team
* Lead teams for special internal projects
* Draft system maintenance emails for hosted customers

**Accomplishments**

* Acted as support team lead for approximately 25% of TimeLink6 customers
* Created 6 Perl based data parsing tools deployed in customer production environments
	* One script saved 2 weeks of development time – meeting otherwise unattainable customer deadline
	* Created another script in 2 days to meet customer deadline – including requirement gathering, development, customer testing, and deployment
* Created two inventory systems with PHP/MySQL
	* Prior to first inventory system, company had no way of tracking devices
	* Second inventory system added data auditing and user configuration

### Student Support / Developer - West Engineering Computing Network - May 2007 to Apr 2008
**Responsibilities**

* Monitor support email queue and ticket system
* Resolve Windows and Linux software issues
* Repair desktop PC hardware
* Update internal documentation wiki
* Work special projects assigned by director

**Accomplishments**

* Wrote server/client Perl scripts to collect information from lab computers and update MySQL database
* Created Perl script to parse DHCPD.conf file and find missing/inaccurate records
* Redesigned department web site in PHP to be easily updated by plain text files
* Wrote PHP script to audit internal wiki
